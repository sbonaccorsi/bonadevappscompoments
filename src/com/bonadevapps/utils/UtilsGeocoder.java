package com.bonadevapps.utils;

import android.location.Address;

public class UtilsGeocoder {

	public static String getCompleteAddressName(Address address) {
		
		String addr = "";
		
		for (int line = 0; line < address.getMaxAddressLineIndex(); line++) {
			if (address.getAddressLine(line) != null) {
				addr += address.getAddressLine(line) + " ";
			}
		}
		return addr;
	}
}
