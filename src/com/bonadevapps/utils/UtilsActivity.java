package com.bonadevapps.utils;

import java.util.List;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.Context;
import android.util.Log;

public class UtilsActivity {

	public static boolean activityIsRunning(Context context, String packageName) {
		
		ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<RunningTaskInfo> tasks = activityManager.getRunningTasks(Integer.MAX_VALUE);
        
        for (RunningTaskInfo task : tasks) {
        	if (packageName.contentEquals(task.baseActivity.getClassName())) {
        		return true;
        	}
        }
        return false;
	}
}
