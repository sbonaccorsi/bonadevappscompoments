package com.bonadevapps.compoments.draw;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.provider.Settings.Global;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.ImageView;

public class CroppingImageView extends ImageView implements OnGlobalLayoutListener {

	private Context			mContext;
	private Bitmap			mBitmap;
	
	private float			mPosLeft;
	private float			mPosTop;
	private float			mPosBottom;
	private float			mPosRight;
	
	private float			mOriginWidth;
	private float			mOriginHeight;
	
	private Paint			mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
	
	public CroppingImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
	}

	public CroppingImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		mContext = context;
	}

	public CroppingImageView(Context context) {
		super(context);
		mContext = context;
	}
	
	private void initializeCroppingRect() {
		
		mOriginHeight = 200;
		mOriginWidth = 200;
		
		float centerX = (getLeft() + getRight()) / 2;
		float centerY = ((ViewGroup)getParent()).getHeight() / 2;
		
//		Log.e("getLeft", "" + getLeft());
//		Log.e("getRight", "" + getRight());
//		Log.e("getTop", "" + getTop());
//		Log.e("getBottom", "" + getBottom());
//		
//		Log.e("centerX", "" + centerX);
//		Log.e("centerY", "" + centerY);
		
//		mPosLeft = centerX - (mOriginWidth / 2);
		mPosLeft = centerX;
//		mPosRight = centerX + (mOriginWidth / 2);
		mPosRight = centerX + mOriginWidth;
//		mPosTop = centerY - (mOriginHeight / 2);
		mPosTop = centerY;
//		mPosBottom = centerY + (mOriginHeight / 2);
		mPosBottom = centerY + mOriginHeight;
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			
			break;

		default:
			break;
		}
		
		return true;
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		
		mPaint.setColor(Color.rgb(255, 136, 0));
		mPaint.setStrokeWidth(5);
		mPaint.setStyle(Paint.Style.STROKE);
		canvas.drawRect(mPosLeft, mPosTop, mPosRight, mPosBottom, mPaint);
	}
	
	public void setImage(Bitmap bitmap) {
		setImageBitmap(bitmap);
		
		refreshDrawableState();
		invalidate();
		
		final ViewTreeObserver tree = getViewTreeObserver();
		if (tree.isAlive()) {
			tree.addOnGlobalLayoutListener(this);
		}
	}

	@Override
	public void onGlobalLayout() {
		getViewTreeObserver().addOnGlobalLayoutListener(null);
		
		Log.e("CroppingImageView Width", "" + getMeasuredWidth());
		Log.e("CroppingImageView Height", "" + getMeasuredHeight());
		
		this.initializeCroppingRect();
		
		refreshDrawableState();
		invalidate();
	}
}
