package com.bonadevapps.compoments.soundplayer;

import java.io.IOException;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Handler;

public class SoundPlayer implements OnCompletionListener {

	private Context						mContext;
	private MediaPlayer					mMediaPlayer;
	private String						mPathLoaded;
	
	private Boolean						mIsPreviewSound;
	private Double						mTimeSound;
	private Double						mCurrentTimeSound;
	private Handler						mHandlerTimeSound;
	
	private PlayerListener				mListener;

	private Runnable					mRunnableTimeSound = new Runnable() {

		@Override
		public void run() {

			if (mCurrentTimeSound >= mTimeSound) {
				stopSound();
			}
			else {
				mCurrentTimeSound++;
				mHandlerTimeSound.postDelayed(mRunnableTimeSound, 1000);
			}
		}
	};

	public SoundPlayer(Context context) {
		mContext = context;
		mIsPreviewSound = false;
		mMediaPlayer = new MediaPlayer();
		mMediaPlayer.setOnCompletionListener(this);
		mHandlerTimeSound = new Handler();
		mCurrentTimeSound = 0d;
	}
	
	public void setPlayerListener(PlayerListener listener) {
		mListener = listener;
	}

	public String getPathLoaded() {
		return mPathLoaded;
	}
	
	public void loadSoundByPath(String path) {
		try {
			mMediaPlayer.setDataSource(path);
			mMediaPlayer.prepare();
			mPathLoaded = path;
			if (mListener != null) {
				mListener.loadedSound();
			}

		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playSound() {

		if (mMediaPlayer.isPlaying()) {
			mMediaPlayer.stop();
			mPathLoaded = null;
			mCurrentTimeSound = 0d;
			mHandlerTimeSound.removeCallbacks(mRunnableTimeSound);
			if (mListener != null) {
				mListener.stopSound();
			}
		}
		mMediaPlayer.start();
		mHandlerTimeSound.postDelayed(mRunnableTimeSound, 1000);
		if (mListener != null) {
			mListener.startSound();
		}
	}

	public void stopSound() {
		if (mMediaPlayer.isPlaying()) {
			mPathLoaded = null;
			mMediaPlayer.stop();
			mMediaPlayer.reset();
			mCurrentTimeSound = 0d;
			mHandlerTimeSound.removeCallbacks(mRunnableTimeSound);
			if (mListener != null) {
				mListener.stopSound();
			}
		}
	}

	public void loadSoundById(Long id) {

	}

	public void loadSoundRessource(Integer ressouce) {

	}

	public void setIsPreviewSound(Double timePreview, Boolean isPreview) {
		mTimeSound = timePreview;
		mIsPreviewSound = isPreview;
	}

	public Boolean isPlaying() {
		return mMediaPlayer.isPlaying();
	}

	@Override
	public void onCompletion(MediaPlayer mp) {
		mPathLoaded = null;
		mp.stop();
		mp.reset();
		mCurrentTimeSound = 0d;
		mHandlerTimeSound.removeCallbacks(mRunnableTimeSound);
		if (mListener != null) {
			mListener.stopSound();
		}
		mMediaPlayer = mp;
	}
	
	public void releasePlayer() {
		mMediaPlayer.release();
	}
}
