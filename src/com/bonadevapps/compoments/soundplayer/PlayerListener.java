package com.bonadevapps.compoments.soundplayer;

public interface PlayerListener {

	public void loadedSound();
	public void startSound();
	public void stopSound();
}
