package com.bonadevapps.compoments.widget.checkable;

import com.bonadevapps.compoments.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.Checkable;
import android.widget.ImageButton;

public class ImageButtonCheckable extends ImageButton implements Checkable {

	private Boolean						mIsChecked = false;
	private Integer						mRessourceChecked;
	private Integer						mRessourceUnChecked;
	
	public ImageButtonCheckable(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		
		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ImageButtonCheckable, defStyle, 0);
		mRessourceChecked = a.getResourceId(R.styleable.ImageButtonCheckable_checked_ressource, 0);
		mRessourceUnChecked = a.getResourceId(R.styleable.ImageButtonCheckable_unchecked_ressource, 0);
		
		this.setImageResource(mRessourceUnChecked);
		
		a.recycle();
	}

	public ImageButtonCheckable(Context context, AttributeSet attrs) {
		super(context, attrs, 0);
	}

	public ImageButtonCheckable(Context context) {
		super(context);
	}

	@Override
	public boolean isChecked() {
		return mIsChecked;
	}

	@Override
	public void setChecked(boolean checked) {
		mIsChecked = checked;
	}

	@Override
	public void toggle() {
		if (mIsChecked == true) {
			mIsChecked = false;
			this.setImageResource(mRessourceUnChecked);
		}
		else {
			mIsChecked = true;
			this.setImageResource(mRessourceChecked);
		}
	}
}
