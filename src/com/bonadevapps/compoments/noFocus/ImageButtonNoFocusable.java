package com.bonadevapps.compoments.noFocus;

import com.bonadevapps.compoments.widget.checkable.ImageButtonCheckable;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewDebug.ExportedProperty;

public class ImageButtonNoFocusable extends ImageButtonCheckable {

	public ImageButtonNoFocusable(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public ImageButtonNoFocusable(Context context, AttributeSet attrs) {
		super(context, attrs, 0);
	}

	public ImageButtonNoFocusable(Context context) {
		super(context);
	}

	@Override
	@ExportedProperty(category = "focus")
	public boolean isFocused() {
		return false;
	}
	
	@Override
	public void setFocusable(boolean focusable) {
		super.setFocusable(false);
	}
}