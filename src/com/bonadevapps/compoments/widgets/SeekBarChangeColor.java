package com.bonadevapps.compoments.widgets;

import com.bonadevapps.compoments.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.SeekBar;

public class SeekBarChangeColor extends SeekBar {

	private Integer				mColorR;
	private Integer				mColorG;
	private Integer				mColorB;
	
	public SeekBarChangeColor(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		
		TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.SeekBarChangeColorAttrs, defStyle, 0);
		mColorR = array.getInteger(array.getResourceId(R.styleable.SeekBarChangeColorAttrs_color_r, 0), 0);
		mColorG = array.getInteger(array.getResourceId(R.styleable.SeekBarChangeColorAttrs_color_g, 0), 0);
		mColorB = array.getInteger(array.getResourceId(R.styleable.SeekBarChangeColorAttrs_color_b, 0), 0);
		
		array.getInteger(array.getResourceId(R.styleable.SeekBarChangeColorAttrs_color_r, 0), 0);
		
		Log.e("SeekBarChangeColor", "Attrs R = " + mColorR);
		Log.e("SeekBarChangeColor", "Attrs G = " + mColorG);
		Log.e("SeekBarChangeColor", "Attrs B = " + mColorB);
		
		array.recycle();
	}

	public SeekBarChangeColor(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public SeekBarChangeColor(Context context) {
		super(context);
	}

}
